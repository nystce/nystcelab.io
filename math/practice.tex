\documentclass[answers]{exam}

\renewcommand{\choicelabel}{(\alph{choice})}
\CorrectChoiceEmphasis{\color{red}}

\let\secpage\clearpage
%\let\secpage\empty

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{datetime2}
\usepackage{siunitx}

\usepackage{pgfplots}
\pgfplotsset{compat=1.16}

\RequirePackage{newpxtext}
\RequirePackage[
euler-digits,
euler-hat-accent,
T1]{%
eulervm}

\RequirePackage[T1]{fontenc}
\RequirePackage[cal=boondoxo]{mathalfa}

\usepackage{enumitem}
\usepackage{geometry}

\usepackage{hyperref}

\begin{document}

\begin{center}
  \LARGE{NYSTCE Mathematics CST (004) review} \\
  \vspace{.25\baselineskip}
  \large{\today}
\end{center}

\vspace{2\baselineskip}

\begin{questions}

  \question
  Use the conjecture below to answer the question that follows.
  \begin{center}
    \fbox{For each prime number $p$ greater than $2$, the number $p^2 + 4$ is also prime.}
  \end{center}
  Which of the following constitutes a counterexample to the conjecture?
  Select all that apply.

  \begin{choices}
    \choice
    $p = 2$
    \choice
    $p = 3$
    \choice
    $p = 9$
    \CorrectChoice
    $p = 11$
  \end{choices}

  \begin{solution}
    The answer is (d).
    \begin{itemize}
      \item
        While $2^2 + 4 = 8$ is not prime,
        $2$ is not greater than $2$,
        so $p = 2$ is not a counterexample.
      \item
        While $3$ is greater than $2$, note that $3^2 + 4 = 13$ is prime,
        so $p = 3$ is not a counterexample.
      \item
        While $9^2 + 4 = 85$ is not prime, note that $9$ is not prime either,
        so $p = 9$ is not a counterexample.
      \item
        As $11$ is a prime number greater than $2$,
        and $11^2 + 4 = 125$ is not prime,
        we see that $p = 11$ is a counterexample.
    \end{itemize}
  \end{solution}

  \question
  Let $P$, $Q$, and $R$ be propositions.
  Suppose that $Q$ is false and that the following proposition is true:
  \begin{center}
    \fbox{ If $P$, then either $Q$ or $\neg R$.}
  \end{center}
  Which of the following propositions can be deduced from these hypotheses?
  Select all that apply.

  \begin{choices}
    \choice
    $\neg P$
    \choice
    $\neg R$
    \CorrectChoice
    If $P$, then $\neg R$.
    \CorrectChoice
    If $R$, then either $\neg P$ or $Q$.
  \end{choices}

  \begin{solution}
    The answer is (c) and (d).
    \begin{itemize}
      \item
        Based on the information we are given,
        the only way to deduce $\neg P$ is via the contrapositive:
        ``If $P$, then either $Q$ or $\neg R$'' is equivalent to its contrapositive ``If $\neg (Q \vee \neg R)$, then $\neg P$.''
        By De Morgan's law, $\neg (Q \vee \neg R)$ is equivalent to $\neg Q \wedge \neg \neg R$.
        Two negations cancel one another out,
        so the latter is equivalent to $\neg Q \wedge R$.
        We know that $Q$ is false, so $\neg Q$ is true.
        So $\neg (Q \vee \neg R)$ is true if and only if $R$ is true.
        We don't know if $R$ is true,
        so we cannot deduce that $\neg P$ is true based on the given information.
      \item
        Since $Q$ is false, $(Q \vee \neg R)$ is true if and only if $\neg R$ is true.
        The if-then statement tells us that $Q \vee \neg R$ is true whenever $P$ is true,
        but we don't know if $P$ is true.
        We therefore do not have enough information to determine whether $\neg R$ is true.
      \item
        Since $Q$ is false, $(Q \vee \neg R)$ is equivalent to $\neg R$,
        so the if-then statement is equivalent to (c).
      \item
        If $R$ is true,
        then $Q \vee \neg R$ is false, since $Q$ is also false.
        The if-then statement then tells us that the hypothesis must be false,
        as the conclusion is false.
        Hence, $\neg P$ must be true if $R$ is true,
        i.e., we can deduce the proposition ``If $R$, then $\neg P$.''
        As $Q$ is false, $\neg P$ is equivalent to $\neg P \vee Q$,
        so we can deduce (d).
    \end{itemize}
  \end{solution}

  \question
  What is the domain of the following rational function?
  \[
    f(x)
    = \frac{7 (x^2 - 25)}{x^2 + 4x -5}
  \]

  \begin{choices}
    \choice
    $(-\infty, -5) \cup (-5, 5) \cup (5, \infty)$
    \CorrectChoice
    $(-\infty, -5) \cup (-5, 1) \cup (1, \infty)$
    \choice
    $(-\infty, 1) \cup (1, \infty)$
    \choice
    all real numbers
  \end{choices}

  \begin{solution}
    The answer is (b).
    The domain of a rational function is the set of all real numbers that are not zeros of the denominator.
    In our case, the denominator $x^2 + 4x - 5$ factors as $(x - 1)(x+5)$,
    so its zeros are $x = 1$ and $x = -5$.
    
    Do not be mislead by the fact that $x + 5$ is also a factor of the numerator.
    Although the two terms cancel away from $x = -5$,
    $f(x)$ is undefined at $x = -5$:
    dividing by zero is impermissible,
    and even the expression $0/0$ is undefined.
  \end{solution}

  \question
  The following graph best describes which of the following functions?

  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[axis lines = center]
        \addplot[color=red, domain=-25:25, samples=900, restrict y to domain = -7:7]{3*(x+2)^2*(x-3)^2*x^(-4)};
      \end{axis}
    \end{tikzpicture}
  \end{center}

  \begin{choices}
    \choice
    $\displaystyle 3(x+2)^2(x-3)^2$
    \CorrectChoice
    $\displaystyle \frac{3(x+2)^2(x-3)^2}{x^4}$
    \choice
    $\displaystyle \frac{(x+2)^2(x-3)^2}{3x^4}$
    \choice
    $\displaystyle \frac{3(x+2)^2(x-3)^2}{x}$
  \end{choices}

  \begin{solution}
    The answer is (b).

    \begin{itemize}
      \item
        Looking at the graph, there appears to be a vertical asymptote near $x = 0$, a horizontal asymptote near $y = 3$, and $x$-intercepts near $x = -2$ and $x = 3$.
      \item
        Each of the choices has zeros at $x = -2$ and $x = 3$,
        so the $x$-interecepts don't allow us to eliminate any choices.
      \item
        The function (a) is polynomial and therefore has no asymptotes.
      \item
        The remaining functions (b), (c), and (d) are rational.
      \item
        Rational functions have vertical asymptotes at the zeros of their denominators.
        Each of (b), (c), and (d) therefore have a vertical asymptote at $x = 0$.
      \item
        If the numerator and denominator of a rational function are of the same degree,
        then that rational function has a horizontal asympote at the ratio of the leading coefficients of the numerator and denominator.
        Thus, (b) has a horizontal asymptote at $y = 3$,
        (c) has a horizontal asymptote at $y = 1/3$, 
        and (d) has no horizontal asymptote as its numerator is of degree $4$,
        while its denominator is of degree $1$.
      \item
        Thus, (b) is the only function with the expected asymptotes.
    \end{itemize}
  \end{solution}

  \question
  The graph of the function
  \[
    f(x) = \frac{2(x^2 - 9)(x + 5)}{x^2 + x - 6}
  \]
  has an asymptote along which of the following lines?
  Select all that apply.

  \begin{choices}
    \choice
    $x = -3$
    \CorrectChoice
    $x = 2$
    \choice
    $y = 2$
    \CorrectChoice
    $y = 2x$
  \end{choices}

  \begin{solution}
    The answer is (b) and (d).

    One might arrive at this answer by plotting the function:
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[axis lines = center]
          \addplot[color=red, domain=-70:70, samples=800, unbounded coords = jump, restrict y to domain = -50:50]{2(x^2 - 9)*(x+5)/(x^2 + x - 6)};
        \end{axis}
      \end{tikzpicture}
    \end{center}

    Alternatively, one could reason algebraically.
    Factoring the numerator and denominator of the rational function,
    we find that
    \[
      f(x)
      = \frac{2 (x + 3) ( x - 3) ( x + 5)}{(x + 3) (x - 2)}.
    \]
    The repeated factor of $x + 3$ in the numerator and denominator tells us that there is a point discontinuity at $x = -3$, but otherwise the graph looks like that of
    \[
      g(x) = \frac{2 (x - 3) (x + 5)}{x-2}.
    \]
    The graph of $g(x)$ has a vertical asymptote at the zero of the denominator, i.e., at $x = 2$,
    so (b) is an asymptote.

    The numerator of $g(x)$ is of degree $2$, while the denominator is of degree $1$.
    As the difference in degrees is therefore $3 -2 = 1$,
    the graph of $g(x)$ will have an oblique asymptote at the ratio of the leading terms: $y = 2x^2/x = 2x$,
    so (d) is also an asymptote.
  \end{solution}

  \question
  The profit function for sales of a particular computer is $P(x) = -0.3 x^2 + 910x - 6350$, where $P(x)$ is the profit in dollars when $x$ units are sold.
  What is the maximum profit that can be realized? 

  \begin{choices}
    \choice
    \$\num{683733.20}
    \CorrectChoice
    \$\num{683733.30}
    \choice
    \$\num{683733.33}
    \choice
    \$\num{683733.333}
  \end{choices}

  \begin{solution}
    The correct answer is (b).

    The profit function is quadratic, 
    i.e., it is of the form $ax^2 + bx + c$ for real numbers $a$, $b$, and $c$.
    Since the leading coefficient $a = -0.3$ is negative,
    $P(x)$ attains its maximum value at
    \[
      x
      = -\frac{b}{2a}
      = -\frac{910}{2(-0.3)}
      = 1516 + \frac{2}{3}.
    \]
    As it wouldn't be logical to sell $\frac{2}{3}$ computers,
    and as the graph of $P(x)$ is symmetric about $x = 1516.\bar{6}$,
    the maximum possible profit will be attained at integer value nearest to $1516.\bar{6}$,
    i.e., at $1517$.
    Plugging $P(1517)$ into a calculator,
    we find $683733.3$,
    and (b) is the correct answer.
  \end{solution}

  \question
  Which of the following represent irrational numbers?
  Select all that apply.

  \begin{choices}
    \CorrectChoice
    the length of the hypotenuse of a right triangle whose remaining two sides are of length $3$ and $9$
    \choice
    the area of a rectangle with sides of length $\sqrt{7}$ and $\sqrt{28}$
    \choice
    the $x$-coordinate of the midpoint of the line segment from $\displaystyle \Big(\frac{2}{9},-\frac{1}{4}\Big)$ to $\displaystyle\Big(-\frac{3}{7}, \frac{1}{5}\Big)$ in the real plane 
  \item
    the area of a circle of diameter $\displaystyle \sqrt{\frac{2}{\pi}}$
  \end{choices}

\begin{solution}
  The answer is (a).
  
  \begin{itemize}
    \item
      By the Pythagorean Theorem,
      the hypotenuse is of length $\sqrt{3^2 + 9^2} = \sqrt{90} = \sqrt{2 \cdot 3^2 \cdot 5} = 3 \sqrt{10}$. 
      Since $10 = 2 \cdot 5$ has no repeated prime factors,
      $\sqrt{10}$ is irrational,
      and $3 \sqrt{10}$ is therefore also irrational.
    \item
      The area of such a rectangle is $\sqrt{7} \cdot \sqrt{28} = \sqrt{7 \cdot 28} = \sqrt{2^2 \cdot 7^2} = 2 \cdot 7 = 14$,
      which is rational.
    \item
      The coordinates of the midpoint are the averages of the coordinates of the endpoints.
      The average of two rational numbers is rational,
      so the $x$-coordinate is again rational here.
    \item
      The area of the circle is 
      \[
        \pi \left(\frac{1}{2} \sqrt{\frac{2}{\pi}}\right)^2
        = \frac{\pi}{4} \left(\frac{2}{\pi} \right)
        = \frac{1}{2},
      \]
      which is rational.
  \end{itemize}
\end{solution}

\question
Consider $n$ square sheets of paper, each with sides of length $S$,
arranged on a table so that they form a chain of square diamonds overlapping in smaller square diamonds,
such that the smaller square diamonds in the overlapping regions each have sides of length $s$.
For example, if $n = 5$, then the chain would resemble the following diagram:
\begin{center}
  \begin{tikzpicture}[scale=.3]
  \draw[gray, thick] (3, 0) -- (0, 3);
  \draw[gray, thick] (0, 3) -- (-3, 0);
  \draw[gray, thick] (-3, 0) -- (0, -3);
  \draw[gray, thick] (0, -3) -- (3, 0);
  \draw[gray, thick] (2, 0) -- (5, 3);
  \draw[gray, thick] (5, 3) -- (8, 0);
  \draw[gray, thick] (8, 0) -- (5, -3);
  \draw[gray, thick] (5, -3) -- (2, 0);
  \draw[gray, thick] (7, 0) -- (10, 3);
  \draw[gray, thick] (10, 3) -- (13, 0);
  \draw[gray, thick] (13, 0) -- (10, -3);
  \draw[gray, thick] (10, -3) -- (7, 0);
  \draw[gray, thick] (12, 0) -- (15, 3);
  \draw[gray, thick] (15, 3) -- (18, 0);
  \draw[gray, thick] (18, 0) -- (15, -3);
  \draw[gray, thick] (15, -3) -- (12, 0);
  \draw[gray, thick] (17, 0) -- (20, 3);
  \draw[gray, thick] (20, 3) -- (23, 0);
  \draw[gray, thick] (23, 0) -- (20, -3);
  \draw[gray, thick] (20, -3) -- (17, 0);
\end{tikzpicture}
\end{center}
If $n > 1$, what is the total area of this chain of larger square diamonds?

\begin{choices}
\choice
$n S^2$
\choice
$n (S^2 - s^2)$
\choice
$2 (S^2 -s^2) + (n-2)(S^2 - 2s^2)$
\CorrectChoice
$S^2 + (n-1) (S + s) (S - s)$
\end{choices}

\begin{solution}
  The answer is (d).

  If $n = 1$, then there is just one square,
  and the area is $S^2$.
  If $A_{n-1}$ is the area of a chain of $n-1$ square diamonds,
  then $A_n = A_{n-1} + (S^2 - s^2)$:
  we add the area of one additional large square, and subtract the area of the additional overlap.
  This means that $A_n$ will be the sum of the area of $n$ large squares 
  mines the area of the $n - 1$ overlaps,
  i.e., $A_n = nS^2 - (n-1)s^2$.
  On the other hand,
  \[
    S^2 + (n-1)(S+s)(S-s)
    = S^2 + (n-1)(S^2 - s^2)
    = S^2 + (n-1)S^2 - (n-1)s^2
    = nS^2 - (n-1)s^2,
  \]
  so (d) is indeed the correct answer.
\end{solution}

\question
For which real values of $a$ will the solution to the system of linear equations
\[
  \left\{\begin{array}{l} y = -2x + 3 \\ y = ax + 5 \end{array}\right.
\]
lie in the fourth quadrant?

\begin{choices}
  \choice
  $\displaystyle a > -2$
  \choice
  $\displaystyle a < -\frac{10}{3}$
  \CorrectChoice
  $\displaystyle -\frac{10}{3} < a < -2$
  \choice
  $\displaystyle -3 < a < -2$
\end{choices}

\begin{solution}
  The answer is (c).

  Graphically, the system looks like this:
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[axis lines = center]
        \addplot[color=red, domain=-5:5, samples=100, restrict y to domain = -3:10]{-2.3*x + 5};
        \addlegendentry{$y = a x + 5$}
        \addplot[color=blue, domain=-5:5, samples=100, restrict y to domain = -3:10]{-2*x + 3};
        \addlegendentry{$y = -2 x + 3$}
        \end{axis}
      \end{tikzpicture}
    \end{center}
    Keep in mind that the slope of the red line is not fixed,
    although its $y$-intercept is fixed at $5$.
    The solution of this system, if it exists, corresponds to the point of intersection of the blue and red lines.
    There are a few cases to consider.
    \begin{itemize}
      \item
        If $a = -2$, then the two lines are parallel and do not intersect,
        so there is no solution in this case.
      \item
        If $a > -2$, then the two lines will intersect in the second quadrant.
      \item
        If the blue and red lines intersect at the same point on the $x$-axis,
        then they both have an $x$-intercept of $x = 1.5$,
        as that is the $x$-intercept of $y = -2x + 3$.
        In that case, the red line must have slope
        \[
          a = \frac{0-5}{1.5}
            = -\frac{10}{3}.
        \]
      \item
        If $a < -10/3$, then the two lines will intersect in the first quadrant.
      \item
        If $-10/3 < a < -2$, the two lines will intersect in the third quadrant.
    \end{itemize}
        Alternatively, we can solve this problem purely algebraically as follows.
        First, compute the $x$-value of the solution in terms of $a$:
        \[
          ax + 5 = -2x + 3
          \Rightarrow (a + 2)x = -2
          \Rightarrow x = \frac{-2}{a+2} \quad\text{and}\quad a \neq -2.
        \]
        If this solution corresponds to a point in the fourth quadrant,
        then the $x$-coordinate must be positive and the $y$-coordinate must be negative,
        so we have the following system of inequalities:
        \[
          \left\{\begin{array}{l}
              0 < \frac{-2}{a+2} \\ 0 > -2 \left(\frac{-2}{a+2} \right) + 3
            \end{array}\right.
        \]
        Solving this system, we find that $a < -2$ and $a > -10/3$.
  \end{solution}

  \question
  Which of the following linear functions $g(x)$ has a graph perpendicular to the graph of $f(x) = 7x - 19$ and satisfies $f(3) = g(3)$?

  \begin{choices}
    \choice
    $\displaystyle g(x) = -\frac{1}{7}x$
    \CorrectChoice
    $\displaystyle g(x) = -\frac{1}{7}x + \frac{17}{7}$
    \choice
    $\displaystyle g(x) = -7x + 23$
    \choice
    $\displaystyle g(x) = \frac{1}{7}x + \frac{11}{7}$
  \end{choices}

  \begin{solution}
    The answer is (b).

    The slope of the graph of $f(x)$ is $7$.
    The slope of a line perpendicular to that one is the negative reciprocal,
    that is, $-1/7$.
    The function $g(x)$ is therefore of the form $g(x) = (-1/7)x + b$ for some $b$.
    To determine $b$, we use the condition that $f(3) = g(3)$:
    \begin{align*}
      7 \cdot 3 - 19
      &= -\frac{3}{7} + b \\
      2 + \frac{3}{7} &= b \\
      \frac{17}{7} &= b.
    \end{align*}
  \end{solution}
\end{questions}
\end{document}
