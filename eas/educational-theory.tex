\section{Educational theory}
\label{theory}

\subsection{Constructivist education theory}
\label{constructivism}

\begin{enumerate}
  \item
    continuing and building upon Piaget's theories of cognitive development (\ref{piaget}),
    \emph{constructivism} is a theory of education that posits that learning is a process through which students construct knowledge individually through the two fundamental processes of \emph{assimilation} and \emph{accommodation}
  \item
    \emph{assimilation} of information is a process in which a learner incorporates new information into a pre-existing framework of conception and understanding
  \item
    \emph{accommodation} of information is a process in which learners reframe their mental representation of the outside world in order to render it compatible with new information that may contradict or be otherwise incompatible with the current framework of conception and understanding
  \item
    implicit in this theory is the fundamental axiom that all students are capable of learning,
    and that the best way to promote learning is by helping students to:
    \begin{enumerate}
      \item
        use their prior knowledge and skills to progressively construct individual understanding of new and increasingly more complex concepts and situations
      \item
        recognize multiple perspectives
      \item
        think critically
      \item
        solve problems
      \item
        work together
    \end{enumerate}
  \item
    in other words,
    constructivists tend to adhere to the philosophy that the purpose of schooling is to teach individuals how to think and learn,
    rather than to fill them with any particular collection of data, facts, and concepts 
  \item
    this conceptualization of the learning process is in many ways the philosophical backbone of the contemporary American education system,
    informing decisions and planning at every level from curricular design to best practices in classroom management
  \item
    in the rest of this section,
    we review some of the key figures and concepts associated with constructivist education theory
  \item
    the rest of this document (Sections \ref{praxis} through \ref{SWD})
    will explore practical implications of constructivist education theory 
  \item
    in our discussion of praxis,
    we will attempt to indicate the theoretical foundations and justifications that gave rise to our contemporary perspectives on the praxis of education with relevant references to Section \ref{theory} whenever possible 
\end{enumerate}

\refs{gubernatis, mometrix, wiki:constructivism}

\subsection{Dewey's progressive education movement}
\label{dewey}

\begin{enumerate}
  \item
    John Dewey (1859--1952) was a philosopher, a psychologist, and an education reformer
  \item
    Dewey promulgated the \emph{progressive education movement}
  \item
    the philosophy behind Dewey's movement was that education should emphasize and focus on creating and understanding experiences,
    rather than rote memorization of mindless facts that are soon forgotten
  \item
    Dewey also believed that school should not be separate from but rather connected with students' lives and life experiences outside the classroom
    in order to facilitate more meaningful and, hence, more memorable and valuable learning experiences
  \item
    Dewey emphasized the importance of active learning, student participation, and classroom democracy,
    rather than authoritarianism and rote methods
    that treat students as empty vessels to be filled with knowledge by an omniscient instructor
\end{enumerate}

\refs{wiki:dewey, wiki:progressive-education}

\subsection{Piaget's theory of cognitive development}
\label{piaget}

\begin{enumerate}
  \item
    Jean Piaget (1896--1980) was a psychologist who studied childhood development
  \item
    Piaget proposed a theory of cognitive development to explain the nature and development of human intelligence
  \item
    according to Piaget's theory, humans pass through the following four stages of development:
    \begin{enumerate}
      \item
        \emph{sensorimotor stage}:
        \begin{enumerate}
          \item
            this stage typically corresponds to the ages of $0$ to $2$ years,
            beginning at birth and lasting until the acquisition of language
          \item
            in this stage, infants construct knowledge by coordinating sensory experiences through physical interactions with objects
        \end{enumerate}
      \item
        \emph{preoperational stage}: 
        \begin{enumerate}
          \item
            this stage typically corresponds to the ages of $2$ to $7$ years
          \item
            during this period, children are not yet able to understand concrete logic,
            to manipulate information mentally,
            or to understand the perspectives of others
          \item
            they are, however, able to form stable concepts and magical beliefs
        \end{enumerate}
      \item
        \emph{concrete operational stage}:
        \begin{enumerate}
          \item
            this stage typically corresponds to the ages of $7$ to $11$ years
          \item
            during this period, a child's thought processes develop and begin to resemble those of an adult
          \item
            the child begins to solve problems logically and perform inductive reasoning
          \item
            nevertheless, the child is not yet able to think hypothetically
            as his/her problem-solving skills are limited in application to concrete situations
        \end{enumerate}
      \item
        \emph{formal operational stage}:
        \begin{enumerate}
          \item
            this is the final stage of development,
            typically lasting from the age of $12$ years to adulthood
          \item
            during this period, the individual is able to use symbols and abstract concepts logically,
            to think hypothetically and metacognitively (\ref{metacognition}),
            and to perform deductive reasoning
        \end{enumerate}
    \end{enumerate}
\end{enumerate}

\refs{gubernatis, wiki:piaget, wiki:piaget-theory}

\subsection{Vygotsky's zone of proximal development}
\label{vygotsky}

\begin{enumerate}
  \item
    Lev Vygotsky (1896--1934) was a psychologist
    who developed a theory of bio-social development
  \item
    the \emph{zone of proximal development (ZPD)},
    as conceptualized by Vygotsky,
    consists of those tasks that a student can accomplish with help,
    but could not accomplish without help
  \item
    this zone is contrasted with two others:
    \begin{enumerate}
      \item
        on the one hand, there is the zone consisting of the tasks that a student could accomplish without help,
        and there's no need for teachers to dwell on tasks that students have already mastered independently,
        and there's no point in attempting to devote instructional time to tasks that students will not be able to master
      \item
        on the other hand, there is the zone consisting of the tasks that the student could not accomplish even with help
    \end{enumerate}
  \item
    the ZPD is the Goldilocks territory in between, and teachers should present activities in the ZPD to provide realistic opportunities for student learning 
\end{enumerate}

\refs{postman, wiki:vygotsky, wiki:ZPD}

\subsection{Bloom's taxonomy of objectives}
\label{bloom}

\begin{enumerate}
  \item
    Benjamin Bloom (1913--1999) was an educational psychologist
  \item
    Bloom introduced a \emph{taxonomy of educational objectives},
    a rubric attempting to classify levels of learning in within each of three categories of learning:
    cognitive, affective, and psychomotor
  \item
    within the cognitive category,
    Bloom's taxonomy consists of the following six levels that allow teachers to classify objectives for their lessons:
    \begin{enumerate}
      \item
        \emph{memorization}:
        remembering specific facts, details, procedures, and recalling vocabulary, terms, and theories
      \item
        \emph{comprehension}:
        understanding or using ideas, but not necessarily relating them to other ideas
      \item
        \emph{application}:
        using concepts in novel situations
      \item
        \emph{analysis}:
        breaking concepts and statements down into component parts, so that their structure may be understood
      \item
        \emph{evaluation}:
        judging and critiquing ideas, concepts, statements according to given criteria
      \item
        \emph{creation}:
        generating new ideas, products, perspectives
    \end{enumerate}
  \item
    this list is ordered from most basic to most advanced,
    and learners must generally proceed from level to level in this order
  \item
    for instance, it is generally not possible demonstrate comprehension without first having memorized the essential terms and facts involved,
    and it's not possible to apply concepts before understanding them,
    etc.
  \item
    teachers should keep these levels in mind while planning instruction,
    starting with activities that help students to master the material at the basic levels of memorization, comprehension, and application,
    followed by activities that allow students to develop and demonstrate capacities for analysis, evaluation, and creation
\end{enumerate}

\refs{gubernatis, postman, wiki:bloom, wiki:bloom-taxonomy}

\subsection{Maslow's hierarchy of needs and human motivation}
\label{maslow}

\begin{figure}[!htb]
  \centering
  \includegraphics[scale=.45]{maslow.pdf}
  \caption{Maslow's hierarchy of needs and human motivations}
  \label{maslow-fig}
\end{figure}

\begin{enumerate}
  \item
    Abraham Maslow (1908--1970) was a psychologist
  \item
    Maslow proposed a hierarchy of the human needs,
    with the idea being that individuals must satisfy certain basic, fundamental needs before they can pursue higher-level needs
  \item
    Maslow's hierarchy is relevant to education insofar as learning, understanding, analytical skills, and imagination all pertain to the higher-level needs,
    and education is not possible unless students' lower-level needs are satisfied
  \item
    Maslow's hierarchy is divided into two classes:
    the lower-level needs,
    which we refer to as the \emph{deficiency needs},
    and the higher-level needs,
    which we refer to as the \emph{being needs}
  \item
    the specific low-level, deficiency needs on Maslow's hierarchy are as follows:
    \begin{enumerate}
      \item
        \emph{biological} and \emph{physiological needs}:
        hunger, thirst, bodily comfort
      \item
        \emph{safety}:
        the feeling of security, the absence of danger
      \item
        \emph{love} and \emph{belonging}:
        the feeling of acceptance and love from others, including from a family
      \item
        \emph{esteem}:
        the feeling that one is respected by others and also by oneself, and regarded as capable and valuable
    \end{enumerate}
  \item
    the specific high-level, being needs on Maslow's hierarchy are as follows:
    \begin{enumerate}
      \item
        \emph{cognitive needs}:
        to know, to understand, to explore
      \item
        \emph{aesthetic needs}:
        to appreciate and to seek out beauty, order, and form
      \item
        \emph{self-actualization}:
        to pursue self-fulfillment, and to realize one's potential
      \item
        \emph{self-transcendence}:
        to help others to self-actualize and to fulfill themselves
    \end{enumerate}
  \item
    the total hierarchy is illustrated in \autoref{maslow-fig}
\end{enumerate}

\refs{postman, wiki:maslow, wiki:maslow-hierarchy}

\subsection{Gardner's multiple intelligences}
\label{gardner}

\begin{enumerate}
  \item
    Howard Gardner (b.\@ 1943) is a developmental psychologist
  \item
    Gardner proposed a \emph{theory of multiple intelligences}, 
    according to which human intelligence operates according to various different modalities,
    as opposed to a single category of ``general ability''
  \item
    Gardner proposes the following list of modalities:
    \begin{enumerate}
      \item
        \emph{visual-spatial thinkers} learn best by visualizing problems;
      \item
        \emph{linguistic thinkers} learn best through words and language;
      \item
        \emph{logical-mathematical thinkers} learn best through abstract, scientific thought and through solving numerical problems
      \item
        \emph{bodily-kinesthetic learners} learn best through physical activity, sports, and dance
      \item
        \emph{musical learners} learn best by listening, singing, and playing musical instruments
      \item
        \emph{interpersonal thinkers} learn best by working with others,
        and they tend to be attuned to the needs of others
      \item
        \emph{intrapersonal thinkers} learn best by working alone,
        and they tend to be introverted and intuitive
      \item
        \emph{naturalistic thinkers} may learn best by relating material to nature and the world around them
      \item
        there may also be \emph{existential thinkers} and \emph{moral thinkers}
    \end{enumerate}
  \item
    there is little empirical data to support Gardner's theory,
    but some educators believe that the theory has practical value 
    insofar as presenting material to students in various ways in accordance with this list of modalities may be more beneficial to students than a one-size-fits-all approach
\end{enumerate}

\refs{postman, wiki:gardner, wiki:gardner-multiple-intelligences}
