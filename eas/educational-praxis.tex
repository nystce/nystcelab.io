\section{Educational praxis}
\label{praxis}

\subsection{Successful learning}

\begin{enumerate}
  \item
    students that are engaged in the learning process are likelier to be successful learners
  \item
    independent work is less conducive to learning that teaching and supervision
  \item
    material and activities of appropriate levels of difficulty are more conducive to effective learning
  \item
    teachers should maintain high expectations for student mastery and utilize instructional time for learning activities to promote successful learning
  \item
    positive classroom environments and constructive criticism are more conducive to learning than negative classroom environments and unconstructive criticism
  \item
    students have positive attitudes towards teachers who offer them warmth, praise, and respect
\end{enumerate}

\refs{postman}

\subsection{Successful teachers}

\begin{enumerate}
  \item
    successful teachers are accepting of children within the teacher-student relationship
  \item
    successful teachers set boundaries and limits that firm, clearly established, and flexible
  \item
    successful teachers are firm and consistent in their enforcement of rules
  \item
    successful teachers establish and clearly communicate positive, realistic expectations for student achievement
  \item
    successful teachers have rational, logical explanations for their expectations for student behavior and achievement,
    and communicate these reasons clearly
  \item
    successful teachers model acceptable behavior
    and hold themselves to the same standards as their students
  \item
    successful teachers do not take students' actions personally
\end{enumerate}

\refs{postman}

\subsection{Student motivation}

\begin{enumerate}
  \item
    beginning lessons with motivation helps to generate student interest and maintain student focus for the duration of the lesson
  \item
    motivation may be either \emph{intrinsic} or \emph{extrinsic}
  \item
    \emph{intrinsic motivation}:
    \begin{enumerate}
      \item
        intrinsic motivation makes topics inherently interesting to, enjoyable for, or popular with students
      \item
        for example, starting a history lesson on a certain historical period by discussing a recent popular movie set in that period would constitute an attempt at providing intrinsic motivation
      \item
        tasks involving individual work provide good opportunities to incorporate intrinsic motivation by appealing to the interests of individual students
    \end{enumerate}
  \item
    \emph{extrinsic motivation}:
    \begin{enumerate}
      \item
        extrinsic motivation is the technique of offering external rewards for students who accomplish established goals, complete tasks well and on time,
      \item
        for example, offering the whole class a trip or a party upon successful completion of a class project would constitute an extrinsic motivation
      \item
        for another example, introducing point or token systems in which students earn and lose points for appropriate and inappropriate activity, respectively, would also be forms of extrinsic motivation
      \item
        extrinsic rewards are most effective when they are offered for goals that most students can achieve
      \item
        when offering extrinsic rewards, take care not to create an unnecessarily competitive environment that is not conducive to optimal learning
      \item
        praise can be used effectively to generate extrinsic motivation
      \item
        praise is most effective as extrinsic motivation when given for specific accomplishments and focused on the student's own behavior rather than on a comparison of the student's behavior with that of the other students in the class
    \end{enumerate}
  \item
    in order to maintain motivation throughout the lesson, consider the following points:
    \begin{enumerate}
      \item
        ensure that the objectives are clear and unambiguous
      \item
        choose tasks that are stimulating and of an appropriate level
      \item
        maintain students' attention
      \item
        allow students some choices throughout the lesson
    \end{enumerate}
  \item
    optimal motivation tends to occur under the following circumstances:
    \begin{enumerate}
      \item
        students enjoy the topic or activity
      \item
        students believe that the lesson is relevant to them
      \item
        students believe that they will succeed
    \end{enumerate}
\end{enumerate}

\refs{postman}

\subsection{Student background knowledge}

\begin{enumerate}
  \item
    connecting lessons and learning objectives with students' prior knowledge and experiences is an effective learning tool
  \item
    by validating students' descriptions of their individual experiences,
    teachers help to motivate students
    and increase their students' self-esteem
  \item
    by starting a lesson by asking students to think metacognitively (\ref{metacognition}) about what they already know
    and what they hope to learn about a certain topic,
    teachers increase student engagement
  \item
    incorporating student-generated questions into the lesson,
    either in place of or in addition to textbook questions,
    and allowing these student-generated questions to guide the lesson,
    is a strategy for motivating students to learn about a given topic
  \item
    moreover, collecting student-generated questions helps teachers to form a pre-assessment of the students' prior knowledge,
    allowing the teacher to tailor the lesson plan to the students' achievement levels,
    and to provide appropriate scaffolding (\ref{scaffolding}) and differentiation (\ref{differentiation})
\end{enumerate}

\refs{mometrix, postman}

\subsection{Scaffolding}
\label{scaffolding}

\begin{enumerate}
  \item
    effective teachers utilize a variety of teaching approaches adapted to the abilities of their students and to their particular lesson objectives
  \item
    effective teachers are constantly aware of the various ways their students develop cognitively
  \item
    \emph{scaffolding} is a technique whose theoretical justification lies in Vygotsky's concept of the zone of proximal development (ZPD),
    as recalled in \ref{vygotsky}
  \item
    in other words, scaffolding is a practical application of the concept of the ZPD within the classroom
  \item
    specifically, \emph{scaffolding} is a technique whereby teachers provide assistance to students to help them to learn material within their individual ZPD
  \item
    as the students master the material,
    the teacher gradually withdraws this help
  \item
    finally, the students no longer need any help and,
    like a building undergoing construction work,
    the scaffolding is completely removed upon completion of the job 
  \item
    effective scaffolding should take the following considerations into account:
    \begin{enumerate}
      \item
        the learning task should guarantee that students employ the skills needed to achieve the learning objectives
      \item
        the learning task should be engaging to maintain student interest and involvement
      \item
        the learning task should be of an appropriate level of difficulty 
      \item
        the teacher should anticipate the errors that the students are likely to make in order to provide appropriate assistance and direction throughout the scaffolding process
      \item
        the assistance provided by the teacher while scaffolding is not limited to cognitive skills:
        emotional support and encouragement is also an important component of scaffolding,
        for example, when confronted with students experiencing frustration or loss of interest during the learning experience
    \end{enumerate}
  \item
    Doug Lemov describes a gradual-release model for scaffolding,
    in which a lesson progresses incrementally from teacher-centered instruction, a teacher-student-collaborative stage, to student practice:
    \begin{enumerate}
      \item
        the process begins with an ``I do'' component,
        in which the teacher provides 
        establishes the objectives of the lesson,
        then provides direct instruction and modeling,
        while students listen, take notes, and ask for clarification when needed
      \item
        the next stage is the ``we do'' component,
        based on interactive instruction,
        with the teacher working with students,
        posing questions, offering prompts and clues,
        providing additional modelling,
        while students ask and respond to questions
        while working with the teacher and their classmates
      \item
        the final stage is the ``you do'' component,
        in which students continue to practice independently and/or in small groups
        while the teacher circulates and provides support
    \end{enumerate}
\end{enumerate}

\refs{gubernatis, lemov, mometrix, postman, wiki:scaffolding}

\subsection{Differentiation}

\subsubsection{Differentiated instruction}
\label{differentiation}

\begin{enumerate}
  \item
    effective teachers are responsive to the individual needs, achievement levels, and backgrounds of their students
  \item
    \emph{differentiation} or \emph{differentiated instruction} is the technique of tailoring instruction according to these individual needs
  \item
    differentiated instruction is a practical response to the educational theories of Dewey (\ref{dewey}),
    Piaget (\ref{piaget}),
    Maslow (\ref{maslow}),
    and Gardner (\ref{gardner})
  \item
    the following are some concrete examples of differentiation:
    \begin{enumerate}
      \item
        \emph{tiered assignments}:
        the teacher provides assignments structured with various levels of abstraction and difficulty to meet varying student needs
      \item
        \emph{learning contracts}:
        teachers give students a freedom in planning within established guidelines for responsible work completion
      \item
        \emph{self-directed learning}:
        students are allowed to make individual choices as to what they want to learn,
        to set individual goals,
        to assume responsibility for work completion,
        to solve problems they encounter during their learning experiences,
        and to perform self-evaluations of their work
      \item
        \emph{problem-based learning}:
        teachers provide students with real-world situations,
        leaving the students to identify the resources and data they will need,
        to solve problems,
        and to choose how to present their findings and demonstrate their learning
      \item
        \emph{seminars}:
        in small groups,
        students explore topics not covered in class,
        or further develop topics presented in class
    \end{enumerate}
  \item
    \emph{pre-assessment} is an important component of differentiated instruction:
    in order to appropriately target instruction to individual student needs,
    the teacher must first determine what those needs are,
    typically by means of an informal test taken before a new unit
\end{enumerate}

\refs{mometrix, wiki:differentiation, wiki:pre-assess}

\subsubsection{Tracking}
\label{tracking}

\begin{enumerate}
  \item
    \emph{tracking} is the practice of assigning students to classes according to their achievement levels,
    e.g., above average, average, below average
  \item
    for example, a school in which students all students in eighth grade take algebra,
    but are placed into either the ``advanced'' class, the standard class, or the ``remedial'' class,
    would be an instance of tracking
  \item
    tracking might be regarded as a very broad form of differentiated instruction,
    but in practice achievement levels within any given tracking level are still quite varied,
    and more individualized differentiation is still required
  \item
    while there is a long tradition of tracking in American schools,
    it is not regarded as an effective strategy by many contemporary education experts
  \item
    among the disadvantages of tracking are:
    \begin{enumerate}
      \item
        implicit racial and social bias and discrimination
      \item
        inherent inequity as high-track classes may tend to attract the most effective teachers and possibly more resources
      \item
        social stigmatization of students in lower tracks
      \item
        tracking tends to be permanent and inflexible:
        it is difficult for students to move from one track to another,
        and so tracking choices made early on, possibly based on data of questionable accuracy or relevance,
        have disproportionately profound effects on each student's education 
    \end{enumerate}
  \item
    from the perspective of the EAS exam,
    tracking and any practices resembling tracking are to be regarded as very poor form
\end{enumerate}

\refs{wiki:tracking}

\subsubsection{Grouping}
\label{grouping}

\begin{enumerate}
  \item
    \emph{grouping} is another broad-scale form of differentiation,
    an alternative to tracking (\ref{tracking})
    that retains some of the benefits,
    in particular, providing gifted students with material adapted to their level of achievement,
    while avoiding the pitfalls of the more rigid tracking system
  \item
    there are various methods for implementing grouping:
    \begin{enumerate}
      \item
        \emph{enrichment clusters}, in which students are grouped by interest,
        possibly with students coming from several different classrooms
      \item
        \emph{cluster grouping}, in which students of similar ability levels are grouped together in a regular classroom
      \item
        within-class flexible grouping
    \end{enumerate}
  \item
    while there is a certain amount of ambiguity in the difference between grouping and tracking,
    proponents of grouping suggest the following:
    \begin{enumerate}
      \item
        grouping should be flexible,
        it should be targeted,
        it should not be permanent,
        and groups are not necessarily based on perceived ability level,
        but possibly based on shared student interests
      \item
        tracking is an inflexible strategy that places students in tracks
        from which they cannot easily move
    \end{enumerate}
  \item
    that said, a rose by any other name\ldots
\end{enumerate}

\refs{mometrix, wiki:gifted, wiki:cluster-grouping}

\subsubsection{Acceleration}
\label{acceleration}

\begin{enumerate}
  \item
    \emph{acceleration} is a strategy that allows gifted students to cover the same curriculum and material with the same level of understanding as other students,
    but in a shorter period of time
  \item
    the following are examples of acceleration:
    \begin{enumerate}
      \item
        Advanced Placement (AP) courses
      \item
        \emph{continuous-progress curriculum with flexible pacing}:
        the curriculum, instructional content, and pace of instruction are adapted to student strengths, needs, and readiness levels as determined through pre-assessment
      \item
        \emph{dual} or \emph{concurrent enrollment}:
        students enroll simultaneously in elementary and middle school,
        middle school and high school,
        or high school and college
      \item
        \emph{curriculum compacting}:
        material that gifted or advanced students have already mastered is eliminated from the lesson plan
        to save time for more challenging learning experiences more adapted to the students' achievement levels
      \item
        \emph{early entrance}:
        students begin school at an earlier age than expected
      \item
        \emph{skipping}:
        students advance two grades at once,
        for example, from second grade to fourth grade
      \item
        \emph{subject acceleration}:
        students take courses at higher grade levels than the rest of their class
    \end{enumerate}
  \item
    beware that skipping, while a traditional strategy for acceleration, is controversial
\end{enumerate}

\refs{mometrix, wiki:gifted, wiki:acceleration}

\subsubsection{Enrichment}
\label{enrichment}

\begin{enumerate}
  \item
    \emph{enrichment} is the practice of providing students with supplemental learning experiences, beyond the established curriculum, in their regular classrooms
  \item
    the following are examples of enrichment strategies:
    \begin{enumerate}
      \item
        academic competitions
      \item
        independent study
      \item
        \emph{learning} or \emph{interest centers}, in which students pursue individual interests within a subject area by using areas within classrooms designated by teachers
      \item
        field trips
      \item
        mentorships
      \item 
        weekend or summer programs, in which students take enrichment classes through public or private organizations or universities
    \end{enumerate}
\end{enumerate}

\refs{mometrix, wiki:gifted}

\subsection{Learning styles}

\begin{enumerate}
  \item
    as recalled in \ref{gardner},
    Gardner's theory of multiple intelligences hypothesizes that human intelligence operates under various modalities
  \item
    although there's an abundance of scientific evidence to the contrary,
    this has led some educators to reason that individuals with predominant intelligence modalities may also have predominant ``learning styles'',
    i.e., that there might be such things as ``visual learners'' or ``auditory learners'' or ``kinesthetic learners''
  \item
    even if we as scientists don't believe in learning styles,
    we are likely to encounter educators who do,
    and it's important to understand their perspectives on education
  \item
    operating under the hypothesis that learning styles exist,
    teachers should adapt their strategies accordingly:
    if students have different learning styles,
    then it is their teachers' responsibility to present material to them in accordance with these different styles so that each student has an equal opportunity to succeed
  \item
    \emph{auditory learning}:
    \begin{enumerate}
      \item
        to accommodate students who learn best by listening to spoken information:
        \begin{enumerate}
          \item
            incorporate e-reader technology and oral reports into the lesson
          \item
            include periods of instruction in which oral expression is as important as written expression
          \item
            encourage students to repeat difficult words and ideas aloud
        \end{enumerate}
    \end{enumerate}
  \item
    \emph{visual-spatial learning}:
    \begin{enumerate}
      \item
        to accommodate students who learn best by reading and viewing tables, charts, and maps:
        \begin{enumerate}
          \item
            consider writing on the board while speaking
          \item
            incorporate a wide selection of written resources and materials
          \item
            encourage students to write reports
          \item
            when a lesson or activity requires complicated instructions,
            provide these instructions both orally and in a typed handout  
        \end{enumerate}
    \end{enumerate}
  \item
    \emph{kinesthetic learning}:
    \begin{enumerate}
      \item
        to accommodate students who learn best through physical activity:
        \begin{enumerate}
          \item
            employ role play or dramatization to introduce concepts
          \item
            encourage students to take notes or to draw sketches or diagrams of what they are hearing in a lesson
          \item
            incorporate hands-on experiences like abaci and tangrams into math lessons
          \item
            incorporate experiments and hands-on experiences into science lessons
          \item
            allow kinesthetic learners to take breaks
        \end{enumerate}
    \end{enumerate}
\end{enumerate}

\refs{postman, wiki:learning-styles}

\subsection{Teacher-centered instructional strategies}

\begin{enumerate}
  \item
    \emph{teacher-centered instruction} is what we might regard as the most traditional form of instruction,
    characterized by teacher presentations,
    factual questions posed by the teacher,
    and knowledge-based responses from students
\end{enumerate}

\subsubsection{Lectures and explanations}

\begin{enumerate}
  \item
    teacher-centered instruction often relies on lectures or explanations provided by the teacher to communicate information to students verbally
  \item
    from the perspective of the constructivist educational theory as espoused by Dewey, Piaget, and Vygotsky,
    lectures and explanations are not ideal methods of instruction:
    they force learners into passive roles
  \item
    this is not to say, however, that they are to be avoided at all costs:
    active-learning-based lessons can require a much greater investment of time from teachers in terms of preparation and planning,
    and less demanding modes of instruction are sometimes practical necessities 
  \item
    to mitigate the inherent passivity of lessons and explanations,
    teachers should consider the following guidelines while lecturing or explaining material to students:
    \begin{enumerate}
      \item
        start the lesson by providing motivation
      \item
        maintain eye contact
      \item
        punctuate the verbal presentation with gesture,
        but without extraneous movements
      \item
        adapt the duration of the presentation to the age of the students
      \item
        have a clear objective
      \item
        make the presentation easy to follow and of an appropriate level
    \end{enumerate}
\end{enumerate}

\refs{gubernatis, postman}

\subsubsection{Demonstrations}

\begin{enumerate}
  \item
    a \emph{demonstration} is a variant of the lecture or explanation in which the teacher models the skill or task that the students are to master
  \item
    for example, a teacher could provide a demonstration of a method for solving a certain type of math problem by modeling the process at the board while students watch
  \item
    demonstrations should adhere to the same guidelines as lectures 
\end{enumerate}

\refs{gubernatis, postman}

\subsubsection{Teacher-posed questions}
\label{teacher-poser-questions}

\begin{enumerate}
  \item
    another standard modality of instruction consists of a teacher posing questions to students during class
  \item
    teacher-posed questions are most effective when they adhere to the following guidelines:
    \begin{enumerate}
      \item
        each question should have a clear purpose
      \item
        questions have are clearly stated, succinct, and of an appropriate level of difficulty 
      \item
        teachers should avoid rhetorical questions
      \item
        keeping Bloom's taxonomy of objectives in mind (\ref{bloom}),
        teachers should ask questions of lower levels (memorization, comprehension, application) \emph{and} higher levels (analysis, evaluation, creation)
      \item
        in particular, teacher's should avoid question-and-answer drills in which the teacher asks an extended series of questions at the memorization level of Bloom's taxonomy,
        as such lines of questioning are too limiting
      \item
        teachers should pause before calling on a student to answer a question:
        this gives all students, including those not chosen to respond, a chance to engage with the question and formulate a response,
        and it also improves the odds that the student chosen to respond will provide a correct or more thoughtful answer
      \item
        teachers should call on a wide range of students,
        rather than simply those that are most or least likely to respond correctly
      \item
        give students several seconds to answer
        and do not cut off students struggling to respond
      \item
        rephrase questions that seem unclear to students
    \end{enumerate}
\end{enumerate}

\refs{postman}

\subsubsection{Self-reflection}
\label{self-reflection}

\begin{enumerate}
  \item
    self-reflection is an essential tool for every teacher,
    to be used in addition to peer feedback in order to ensure continual improvement of teaching skills
  \item
    teachers should consider recording and analyzing everything that occurs during a lesson
    to identify any potential flaws in their teaching strategies
  \item
    post-lesson journals and video-recordings are two effective means of collecting the data upon which self-reflection should be based
  \item
    student observations are another good source of information,
    in the form of questionnaires or surveys
  \item
    care is needed when crafting student surveys
  \item
    when engaging in self-reflection,
    consider the following questions:
    \begin{enumerate}
      \item
        Did the students understand the lesson?
      \item
        Was the lesson of an appropriate level of difficulty?
      \item
        Which learning materials works effectively in this lesson?
      \item
        Which learning materials did not work effectively in this lesson?
      \item
        Did the learning materials maintain student engagement?
      \item
        Did students remain on task during the lesson?
      \item
        Which were the most and least engaging parts of the lesson?
      \item
        Were my directions clear?
      \item
        Was my pace appropriate?
      \item
        Did all students participate?
      \item
        Overall, how effective was the lesson?
      \item
        Were all the learning objectives for this lesson met?
      \item
        In what ways could I improve the lesson for next time?
    \end{enumerate}
\end{enumerate}

\refs{mometrix}

\subsection{Student-centered instructional strategies}

\begin{enumerate}
  \item
    in \emph{student-centered} or \emph{active-learning environments},
    the teacher is no longer the sole source of information
  \item
    rather, in these environments, learning takes on a more democratic form,
    as espoused by Dewey (\ref{dewey})
  \item
    in active-learning environments, teachers tend to pose open-ended, indirect questions
  \item
    in active-learning environments,
    students are encouraged to be active participants in class
    and to create knowledge rather than simply absorbing it
  \item
    student-posed questions,
    encouragement from the teacher,
    and the teacher's incorporation of student-generated ideas into the lesson
    are all fundamental components of active-learning environments
  \item
    while student involvement is a necessary component of active learning,
    beware that it is not sufficient in itself to create an active-learning environment
  \item
    for example, teacher-posed question drills mentioned in \ref{teacher-poser-questions} rely on student involvement,
    but they are not an example of active learning
\end{enumerate}

\refs{postman}

\subsubsection{Metacognition}
\label{metacognition}

\begin{enumerate}
  \item
    \emph{metacognition} is the awareness and understanding of one's own cognition and thought processes
  \item
    metacognition is an important part of active learning
  \item
    metacognition is a useful tool for students in problem-solving situations as it allows students to:
    \begin{enumerate}
      \item
        perceive important information about the problem
      \item
        determine whether they have solved such problems before and by what means
      \item
        determine what strategies they have mastered that might be applicable to the problem
      \item
        determine what relevant contextual knowledge they have
    \end{enumerate}
  \item
    teachers can encourage students to think metacognitively
    by, for example, suggesting that students pursue a line of self-questioning:
    \begin{enumerate}
      \item
        Have I solved problems like this before?
      \item
        What do I already know about this subject?
    \end{enumerate}
  \item
    teachers can also encourage students to think metacognitively by creating flowcharts and concept maps
\end{enumerate}

\refs{gubernatis, postman, wiki:metacognition}

\subsubsection{Cooperative learning}

\begin{enumerate}
  \item
    \emph{cooperative learning} is a modality in which groups of two to six students work together to master skills, learn concepts, or complete projects 
  \item
    these groups are either assigned a specific learning task or project by the teacher,
    or they choose their own
  \item
    the group then devises a plan for working together to complete the task in consultation with the teacher
  \item
    students use resources, including the teacher, and mutual assistance
    assuming responsibilities for subtasks as they work towards their objective 
  \item
    typically, upon completion, students will summarize their efforts,
    and make a presentation to the class or the teacher
  \item
    cooperative learning is a good example of active learning
  \item
    cooperative learning promotes full participation and democracy
  \item
    the following are some standard examples of cooperative-learning activities:
    \begin{enumerate}
      \item
        \emph{tea party}: 
        \begin{enumerate}
          \item
            students form two facing lines
          \item
            the teacher asks a question
          \item
            students discuss answers in facing pairs for one minute
          \item
            one line moves sideways by one to form new facing pairs
          \item
            repeat
        \end{enumerate}
      \item
        think-pair-share
      \item
        round robin
    \end{enumerate}
\end{enumerate}

\refs{mometrix, postman, wiki:cooperative-learning}

\subsubsection{Inquiry-based learning}

\begin{enumerate}
  \item
    \emph{inquiry-based learning} is a modality in which students learn concepts, solve problems, or discover relationships through by following their own thought processes 
  \item
    this modality is often the most demanding in terms of the teacher's preparation
  \item
    in particular, it's important that the teacher be confident that the subject in question will lead to useful results
  \item
    typically, the process begins with the teacher explaining inquiry procedures to students through examples
  \item
    the teacher then presents the problem or situation to be studied
  \item
    students then gather information and ask questions to gain further information
  \item
    once students have completed their inquiry process,
    the teacher should encourage students to think metacognitively (\ref{metacognition}) by asking them to contemplate their process and summarize it
\end{enumerate}

\refs{postman, wiki:inquiry-based-learning}

\subsection{Learning tasks}

\begin{enumerate}
  \item
    effective lessons rely on many different types of activities and tasks
  \item
    the following are some of the most common activities and tasks:
    \begin{enumerate}
      \item
        \emph{critical thinking}:
        mental exercises involving logic and reasoning,
        in particular those relying on comparison, classification, causation, patterns, sequences, analogy, deductive and inductive arguments, hypothesizing, critique
      \item
        \emph{creative thinking}:
        mental exercises involving the creation of new ideas, concepts, arguments, art,
        in particular those relying on brainstorming, elaboration, modification, imagination, associative thought, metaphor
      \item
        \emph{problem solving}:
        tasks in which students apply critical and creative thinking skills to the solution of a fixed problem
      \item
        \emph{invention}:
        tasks in which students apply creative thinking skills to create something new or to improve on something that exists
      \item
        \emph{memorization}:
        tasks at the lowest level of Bloom's taxonomy (\ref{bloom}),
         possibly incorporating mnemonic devices
      \item
        \emph{concept mapping}:
        a task in which students create or examine a graphical representation of relations between concepts or some other organization of concepts,
        such as a flow chart
      \item
        \emph{project}:
        a task in which students explore a certain topic,
        rather than a specific skill or a specific concept,
        and, in the process, develop a variety of skills and ideas pertaining to the given topic
      \item
        \emph{community experts}:
        tasks in which members of the community, possibly but not necessarily parents or guardians of students in the class,
        share their expertise in a particular field to help students to learn
      \item
        \emph{primary resources}:
        tasks in which students learn about a subject by examining original documents or artifacts
        rather than descriptions thereof
    \end{enumerate}
\end{enumerate}

\refs{postman, wiki:concept-map}

\subsection{Looping}
\label{looping}

\begin{enumerate}
  \item
    students perform better when teachers exhibit caring, nurturing attitudes towards them
  \item
    \emph{looping} is a method for promoting caring educational environments
  \item
    specifically, looping is a system in which a teacher will work with the same class of students for two or more years
  \item
    for example, a teacher might teach a group of fourth-grade students one year,
    teach the same group of students in fifth grade the following year,
    then ``loop'' back to a new group of fourth-grade students the year after
  \item
    looping has the following benefits:
    \begin{enumerate}
      \item
        it allows teachers to learn more about student strengths, needs, interests as the looping teacher has more time to get to know each student
      \item
        it helps to foster trust both between classmates and between students and teachers
      \item
        it allows teachers to implement more effective differentiation techniques and more individualized instruction as they get to know students better 
    \end{enumerate}
\end{enumerate}

\refs{mometrix, wiki:inquiry-based-learning}
